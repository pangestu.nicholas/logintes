from django.shortcuts import render,redirect
from .forms import formUser,loginUser
import datetime
from django.contrib import messages
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login,logout,authenticate

def home(request):
    if request.method=="POST":
        form=AuthenticationForm(request,data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username','')
            password = form.cleaned_data.get('password','')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request,user)
                messages.info(request,f"Congratulations, you're login now{username}")
                return render(request,'lobby.html',{
                    "message":username
                })
            else:
                messages.error(request,"There is some error in username or password")

        else:
            messages.error(request,"There is some error in username or password")
    form=AuthenticationForm()
    return render(request,'home.html',{"form":form})

def regis(request):
    
    
    if(request.method == "POST"):
        form=formUser(request.POST)
        if form.is_valid():
            
            user =form.save(commit=False)
            username=request.Post.cleaned_data.get("username")
            email=request.Post.cleaned_data.get("email")
            password=request.Post.cleaned_data.get("password")
            # User.objects.create_user(username, password=password, **validated_data)
            user.save()
            return redirect("lobby")
        else:
            return redirect("/")
        
        
    form = formUser()

    # list_status = Statuss.objects.all()
    
        
    
    context = {
        'formRegis':form,
        # 'list_status': list_status
    }
    return render(request,"regis.html",context)

def lobby(request):
    return render(request,"lobby.html")

def logout_req(request):
    logout(request)
    return redirect("applogin:home")