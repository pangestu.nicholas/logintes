from django import forms
from django.contrib.auth.models import User

class formUser(forms.ModelForm):
    username=forms.CharField(widget=forms.TextInput(attrs={'placeholder' :'Masukan Username','id':'forms','id':'username'}))
    email=forms.CharField(widget=forms.TextInput(attrs={'placeholder' :'Masukan E-mail','class':'forms','id':'email'}))
    password=forms.CharField(widget=forms.PasswordInput(attrs={'placeholder' :'Masukan Password','class':'forms','id':'password'}))
    class Meta:
        model=User
        fields=['username','email','password']

class loginUser(forms.Form):
    username=forms.CharField(widget=forms.TextInput(attrs={'placeholder' :'Masukan Username','id':'forms','id':'username'}))
    password=forms.CharField(widget=forms.PasswordInput(attrs={'placeholder' :'Masukan Password','class':'forms','id':'password'}))
    class Meta:
        model=User
        fields=['username','password']