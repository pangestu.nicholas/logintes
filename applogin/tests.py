from django.test import TestCase,Client
from .views import home, lobby,regis
from django.urls import resolve
# Create your tests here.
class unitTest(TestCase):
    def test_url_is_exist(self):
        response=Client().get('/')
        self.assertEqual(response.status_code,200)

        response=Client().get('/regis/')
        self.assertEqual(response.status_code,200)

        response=Client().get('/lobby/')
        self.assertNotEqual(response.status_code,404)
        self.assertEqual(response.status_code,200)

        

    def test_template_found(self):
        response=Client().get('/')
        self.assertTemplateUsed(response,"home.html")

        response=Client().get('/regis/')
        self.assertTemplateUsed(response,"regis.html")

        response=Client().get('/lobby/')
        self.assertTemplateUsed(response,"lobby.html")

    def test_function_found(self):
        response=resolve('/')
        self.assertEqual(response.func,home)

        response=resolve('/regis/')
        self.assertEqual(response.func,regis)

        response=resolve('/lobby/')
        self.assertEqual(response.func,lobby)