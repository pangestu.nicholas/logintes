from django.urls import path
from .views import home,regis,lobby,logout_req

app_name="applogin"

urlpatterns = [
    path('',home,name="home"),
    path('regis/',regis,name="registrasi"),
    path('lobby/',lobby,name="lobby"),
    path('logout/',logout_req,name="logout")
]
